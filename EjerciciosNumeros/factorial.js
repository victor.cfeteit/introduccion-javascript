/* Programa una función que calcule el factorial de un número 
(El factorial de un entero positivo n, se define como el producto 
de todos los números enteros positivos desde 1 hasta n), 
pe. miFuncion(5) devolverá 120. */

function factorial(num){
    let numero = 1
    for(let x=1;x <= num;x++){
        numero = x * (1+x)
    }
    console.log(numero)
}
factorial(3)